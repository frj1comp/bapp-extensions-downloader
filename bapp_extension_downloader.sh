#!/bin/bash

#Author: Jan Friedli

echo " ▄▄▄▄    ▄▄▄       ██▓███   ██▓███     ▓█████ ▒██   ██▒▄▄▄█████▓▓█████ ███▄    █   ██████  ██▓ ▒█████   ███▄    █   ██████ "
echo "▓█████▄ ▒████▄    ▓██░  ██▒▓██░  ██▒   ▓█   ▀ ▒▒ █ █ ▒░▓  ██▒ ▓▒▓█   ▀ ██ ▀█   █ ▒██    ▒ ▓██▒▒██▒  ██▒ ██ ▀█   █ ▒██    ▒ "
echo "▒██▒ ▄██▒██  ▀█▄  ▓██░ ██▓▒▓██░ ██▓▒   ▒███   ░░  █   ░▒ ▓██░ ▒░▒███  ▓██  ▀█ ██▒░ ▓██▄   ▒██▒▒██░  ██▒▓██  ▀█ ██▒░ ▓██▄   "
echo "▒██░█▀  ░██▄▄▄▄██ ▒██▄█▓▒ ▒▒██▄█▓▒ ▒   ▒▓█  ▄  ░ █ █ ▒ ░ ▓██▓ ░ ▒▓█  ▄▓██▒  ▐▌██▒  ▒   ██▒░██░▒██   ██░▓██▒  ▐▌██▒  ▒   ██▒"
echo "░▓█  ▀█▓ ▓█   ▓██▒▒██▒ ░  ░▒██▒ ░  ░   ░▒████▒▒██▒ ▒██▒  ▒██▒ ░ ░▒████▒██░   ▓██░▒██████▒▒░██░░ ████▓▒░▒██░   ▓██░▒██████▒▒"
echo "░▒▓███▀▒ ▒▒   ▓▒█░▒▓▒░ ░  ░▒▓▒░ ░  ░   ░░ ▒░ ░▒▒ ░ ░▓ ░  ▒ ░░   ░░ ▒░ ░ ▒░   ▒ ▒ ▒ ▒▓▒ ▒ ░░▓  ░ ▒░▒░▒░ ░ ▒░   ▒ ▒ ▒ ▒▓▒ ▒ ░"
echo "▒░▒   ░   ▒   ▒▒ ░░▒ ░     ░▒ ░         ░ ░  ░░░   ░▒ ░    ░     ░ ░  ░ ░░   ░ ▒░░ ░▒  ░ ░ ▒ ░  ░ ▒ ▒░ ░ ░░   ░ ▒░░ ░▒  ░ ░"
echo " ░    ░   ░   ▒   ░░       ░░             ░    ░    ░    ░         ░     ░   ░ ░ ░  ░  ░   ▒ ░░ ░ ░ ▒     ░   ░ ░ ░  ░  ░  "
echo " ░            ░  ░                        ░  ░ ░    ░              ░  ░        ░       ░   ░      ░ ░           ░       ░  "
echo "      ░                                                                                                                    "
echo "▓█████▄  ▒█████   █     █░███▄    █ ██▓     ▒█████  ▄▄▄      ▓█████▄ ▓█████  ██▀███                                        "
echo "▒██▀ ██▌▒██▒  ██▒▓█░ █ ░█░██ ▀█   █▓██▒    ▒██▒  ██▒████▄    ▒██▀ ██▌▓█   ▀ ▓██ ▒ ██▒                                      "
echo "░██   █▌▒██░  ██▒▒█░ █ ░█▓██  ▀█ ██▒██░    ▒██░  ██▒██  ▀█▄  ░██   █▌▒███   ▓██ ░▄█ ▒                                      "
echo "░▓█▄   ▌▒██   ██░░█░ █ ░█▓██▒  ▐▌██▒██░    ▒██   ██░██▄▄▄▄██ ░▓█▄   ▌▒▓█  ▄ ▒██▀▀█▄                                        "
echo "░▒████▓ ░ ████▓▒░░░██▒██▓▒██░   ▓██░██████▒░ ████▓▒░▓█   ▓██▒░▒████▓ ░▒████▒░██▓ ▒██▒                                      "
echo " ▒▒▓  ▒ ░ ▒░▒░▒░ ░ ▓░▒ ▒ ░ ▒░   ▒ ▒░ ▒░▓  ░░ ▒░▒░▒░ ▒▒   ▓▒█░ ▒▒▓  ▒ ░░ ▒░ ░░ ▒▓ ░▒▓░                                      "
echo " ░ ▒  ▒   ░ ▒ ▒░   ▒ ░ ░ ░ ░░   ░ ▒░ ░ ▒  ░  ░ ▒ ▒░  ▒   ▒▒ ░ ░ ▒  ▒  ░ ░  ░  ░▒ ░ ▒░                                      "
echo " ░ ░  ░ ░ ░ ░ ▒    ░   ░    ░   ░ ░  ░ ░   ░ ░ ░ ▒   ░   ▒    ░ ░  ░    ░     ░░   ░                                       "
echo "   ░        ░ ░      ░            ░    ░  ░    ░ ░       ░  ░   ░       ░  ░   ░                                           "
echo " ░                                                            ░                      "

bapp_site_src=$(curl -s https://portswigger.net/bappstore)
extension_ids=$(echo "$bapp_site_src" | grep -E -i "\/bappstore\/[0-9a-f]{32}" | cut -d '/' -f3 | cut -d '"' -f1)
nr_extensions=$(echo "$extension_ids" | wc -l)

extract_extension_name () {
    echo $(echo "$1" | grep "$2" | cut -d ">" -f2 | cut -d "<" -f1)
}

echo -e "[*] Downloading $nr_extensions  BApp Extensions \n"

while IFS= read -r id; do
    extension_name=$(extract_extension_name "$bapp_site_src" "$id" | tr -s " " "_")
    echo "[*] Processing: $extension_name  https://portswigger-cdn.net/bappstore/bapps/download/$id"
    curl -L -o $extension_name.bapp -s https://portswigger-cdn.net/bappstore/bapps/download/$id
done <<< "$extension_ids"

echo -e "\n [*] Creating Zip bapps.zip with password infected"

zip -q -9 -P infected bapps.zip ./*.bapp

echo "[*] Done"
